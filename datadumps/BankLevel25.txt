Automatically generated (n = 200514)
==================================
Cereals
MIN: 1,12851032059286
AVG: 28,1492268929515
MED: 26,043776057977
MAX: 134,833965297681
VAL: 34
==================================
==================================
Chocolate
MIN: 1,2886200901628
AVG: 35,2145077973163
MED: 33,8214661355613
MAX: 164,798372141411
VAL: 44
==================================
==================================
Butter
MIN: 1,61226107930317
AVG: 44,5252436311035
MED: 44,5432742957138
MAX: 154,751051852929
VAL: 54
==================================
==================================
Sugar
MIN: 1,83632762029174
AVG: 53,4752285997213
MED: 58,0979622946404
MAX: 150,052884145909
VAL: 64
==================================
==================================
Nuts
MIN: 1,96684425742871
AVG: 62,1773012591066
MED: 66,463381293764
MAX: 148,817963415475
VAL: 74
==================================
==================================
Salt
MIN: 2,09173737528324
AVG: 69,6815930545171
MED: 74,5026992523043
MAX: 154,231295678938
VAL: 84
==================================
==================================
Vanilla
MIN: 2,4243563928233
AVG: 75,7624522915777
MED: 82,5444215241885
MAX: 172,699320450658
VAL: 94
==================================
==================================
Eggs
MIN: 2,66571388686512
AVG: 88,8112958293387
MED: 97,6848265460924
MAX: 181,812312978045
VAL: 104
==================================
==================================
Cinnamon
MIN: 2,44163944757381
AVG: 98,7157792088749
MED: 108,022947629895
MAX: 184,155880292157
VAL: 114
==================================
==================================
Cream
MIN: 2,95466242171363
AVG: 104,286001371124
MED: 115,591140124473
MAX: 189,346891097623
VAL: 124
==================================
==================================
Jam
MIN: 4,35776571026513
AVG: 115,927271419707
MED: 124,712217414548
MAX: 184,436021397053
VAL: 134
==================================
==================================
White chocolate
MIN: 6,60903949375896
AVG: 123,378434920358
MED: 131,582037616874
MAX: 191,196713422625
VAL: 144
==================================
==================================
Honey
MIN: 19,3395604362417
AVG: 135,742702607682
MED: 149,200987456185
MAX: 194,517687439113
VAL: 154
==================================
==================================
Cookies
MIN: 30,6997455480322
AVG: 141,564175343241
MED: 150,194381934951
MAX: 202,455001156378
VAL: 164
==================================
==================================
Recipes
MIN: 32,6250564349613
AVG: 150,167249594777
MED: 162,378500167736
MAX: 219,195898832378
VAL: 174
==================================